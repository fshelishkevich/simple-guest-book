<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\CommentRequest;
use App\User;

class CommentController extends Controller
{
    public function store(CommentRequest $r)
    {
        $user = auth()->check() ? auth()->user() : User::whereEmail($r->email)->first();

        if (!$user) {
            $user = factory(User::class)->create([
                'email' => $r->email
            ]);
        }

        Comment::create([
            'user_id' => $user->id,
            'body' => $r->body
        ]);

        session()->flash('comment_status', 'created');
        return redirect()->back();
    }

    public function destroy($id)
    {
        Comment::destroy($id);

        return redirect()->back();
    }
}
