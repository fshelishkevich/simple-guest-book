<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class MainPageController extends Controller
{
    public function index()
    {
        $comments = Comment::with('user')
            ->orderBy('id', 'desc')
            ->paginate(5);

        $isAdmin = auth()->check() ? auth()->user()->isAdmin() : null;

        return view('main.index', compact('comments', 'isAdmin'));
    }
}
