<?php

namespace App\Http\Controllers;

use App\Comment;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function banUser($id){
        $user = User::findOrFail($id);
        $user->is_banned = true;
        $user->save();

        return redirect()->back();
    }

    public function unbanUser($id){
        $user = User::findOrFail($id);
        $user->is_banned = false;
        $user->save();

        return redirect()->back();
    }

    public function profile()
    {
        $comments = Comment::whereUserId(auth()->id())
            ->orderBy('id', 'DESC')
            ->paginate(15);

        return view('user.profile', compact('comments'));
    }
}
