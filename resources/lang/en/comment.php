<?php

return [
    'comment_was_saved' => 'Comment was saved',
    'delete_comment' => 'Delete comment',
    'email' => 'Email',
    'enter_email' => 'Enter email',
    'comment' => 'Comment',
    'leave_comment' => 'Leave comment',
    'enter_comment' => 'Enter comment',
    'ban_user' => 'Ban user',
    'unban_user' => 'Unban user',
    'profile' => 'Profile',
];
