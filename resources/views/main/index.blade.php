@extends('layouts.app')

@section('content')
    @if(!auth()->check() || auth()->check() && !auth()->user()->is_banned)
        <!-- Form -->
        <div class="pa-md">
            @if(session()->get('comment_status') == 'created')
                <div class="alert alert-primary" role="alert">
                    @lang('comment.comment_was_saved')
                </div>
            @endif

            <form action="{{ route('comments.store') }}" method="POST">
                @csrf
                @guest()
                    <div class="form-group">
                        <label for="exampleInputEmail1">@lang('comment.email')</label>
                        <input type="email" name="email" value="{{ old('email') }}"
                               class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                               placeholder="@lang('comment.enter_email')">
                        @error('email')
                        <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                @endguest

                <div class="form-group">
                    <label for="exampleFormControlTextarea1">@lang('comment.comment')</label>
                    <textarea class="form-control{{ $errors->has('body') ? ' is-invalid' : '' }}" name="body" rows="3"
                              placeholder="@lang('comment.enter_comment')"></textarea>
                    @error('body')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <button class="btn btn-primary" type="submit">
                    @lang('comment.leave_comment')
                </button>
            </form>
        </div>
    @endif

    <!-- Comments -->
    @foreach($comments as $comment)
        <div class="pa-md">
            <p>{{ $comment->body }}</p>

            <!-- Ban user -->
            @if($isAdmin && !$comment->user->is_banned)
                <form class="float-right" action="{{ route('users.ban', $comment->user->id) }}" method="POST">
                    @csrf
                    <button class="btn btn-danger">@lang('comment.ban_user')</button>
                </form>
            @endif

        <!-- Unban user -->
            @if($isAdmin && $comment->user->is_banned)
                <form class="float-right" action="{{ route('users.unban', $comment->user->id) }}" method="POST">
                    @csrf
                    <button class="btn btn-primary">@lang('comment.unban_user')</button>
                </form>
            @endif
            <div class="float-right pa-md">{{ $comment->user->email }}</div>

            <hr class="clear-both"/>

            @if($isAdmin)
                <form action="{{ route('comments.destroy', $comment->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger">@lang('comment.delete_comment')</button>
                </form>
            @endif
        </div>
    @endforeach

    <!-- Pagination -->
    <div class="pa-md">
        {{ $comments->links() }}
    </div>
@endsection
