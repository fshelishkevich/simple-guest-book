@extends('layouts.app')

@section('content')
    @foreach($comments as $comment)
        <div class="pa-md">
            <p>{{ $comment->body }}</p>
            <hr class="pt-md"/>
        </div>

        {{ $comments->links() }}
    @endforeach
@endsection
