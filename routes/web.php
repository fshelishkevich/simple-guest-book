<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'as' => 'main',
    'uses' => 'MainPageController@index'
]);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::delete('/comments/{id}', [
    'as' => 'comments.destroy',
    'uses' => 'CommentController@destroy',
    'middleware' => 'admin'
]);

Route::resource('comments', 'CommentController')
    ->only(['index', 'store']);

Route::post('/ban-user/{id}', [
    'as' => 'users.ban',
    'uses'=> 'UserController@banUser',
    'middleware' => 'admin'
]);

Route::post('/unban-user/{id}', [
    'as' => 'users.unban',
    'uses'=> 'UserController@unbanUser',
    'middleware' => 'admin'
]);

Route::get('users/profile', [
    'as' => 'users.profile',
    'uses' => 'UserController@profile',
    'middleware' => 'auth'
]);
